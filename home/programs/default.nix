{
  config,
  pkgs,
  ...
}: {
  imports = [
    ./common.nix
    ./zsh.nix
  ];
}
