{pkgs, ...}: {

  programs.zsh = {
    enable = true;
    enableAutosuggestions = true;
    syntaxHighlighting.enable = true;
    enableCompletion = true;
    oh-my-zsh = {
     enable = true;
     theme = "robbyrussell";
     plugins = [ "git" "z" "vscode" ];
    };
    shellAliases = {
      ls = "eza -al --group-directories-first";
      ll = "eza -l --group-directories-first";
      la = "eza -a --group-directories-first";
      lt = "eza -lt --group-directories-first";

      cat = "bat -pp --theme=\"OneHalfDark\"";

      j = "z";

      nixos-switch = "sudo nixos-rebuild switch --upgrade";

      rebuild-system = "sudo nixos-rebuild switch --flake ~/nixos-config";

      ytdl = "youtube-dl -f \"bestvideo[ext=mp4]+bestaudio[ext=m4a]/bestvideo+bestaudio\" --merge-output-format mp4";
    };
    envExtra = "eval \"$(direnv hook zsh)\"";
  };

}
