{pkgs, ...}: {

  # TODO: Figure out how to combine
  # home.packages = with pkgs-stable; [];

  home.packages = with pkgs; [
    neofetch
    nnn # terminal file manager

    # archives
    zip
    xz
    unzip
    p7zip

    # utils
    ripgrep # recursively searches directories for a regex pattern
    jq # A lightweight and flexible command-line JSON processor
    yq-go # yaml processer https://github.com/mikefarah/yq
    eza # A modern replacement for ‘ls’
    fzf # A command-line fuzzy finder

    # networking tools
    mtr # A network diagnostic tool
    iperf3
    dnsutils  # `dig` + `nslookup`
    ldns # replacement of `dig`, it provide the command `drill`
    aria2 # A lightweight multi-protocol & multi-source command-line download utility
    socat # replacement of openbsd-netcat
    nmap # A utility for network discovery and security auditing
    ipcalc  # it is a calculator for the IPv4/v6 addresses

    # Programs
    obsidian

    # misc
    file
    which
    tree
    gnused
    gnutar
    gawk
    zstd
    gnupg
    youtube-dl
    brave
    _1password-gui
    speedtest-cli

    # nix related
    #
    # it provides the command `nom` works just like `nix`
    # with more details log output
    nix-output-monitor

    # productivity
    glow # markdown previewer in terminal

    btop  # replacement of htop/nmon
    iotop # io monitoring
    iftop # network monitoring

    # Programming
    deno

    # system call monitoring
    strace # system call monitoring
    ltrace # library call monitoring
    lsof # list open files

    # system tools
    sysstat
    lm_sensors # for `sensors` command
    ethtool
    pciutils # lspci
    usbutils # lsusb

    zellij
    lazygit
    prusa-slicer
    cura
    keymapp
    chromium
    wl-clipboard
    pamixer
    pavucontrol
    overskride
    ardour
    bitwig-studio
    yabridge
    fluidsynth
    qsynth
    rar
    orca-slicer
    dunst
    transmission
    vlc
    mpv
  ];

    # starship - an customizable prompt for any shell
  # programs.starship = {
  #   enable = true;
  #   # custom settings
  #   settings = {
  #     add_newline = false;
  #     aws.disabled = true;
  #     gcloud.disabled = true;
  #     line_break.disabled = true;
  #   };
  # };

  programs.bat = {
    enable = true;
    config = {
      theme = "OneHalfDark";
    };
  };

  programs.git = {
    enable = true;
    userName = "Dennis Wenger";
    userEmail = "denniswenger10@gmail.com";
  };

  programs.vscode = {
    enable = true;
  };

}
