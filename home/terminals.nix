{ pkgs, ... }:

# terminals

let
  font = "JetBrainsMono Nerd Font";
in
{
  programs.alacritty = {
    enable = true;
    settings = {
      env.TERM = "xterm-256color";
      window.opacity = 0.98;
#      window.dynamic_padding = true;
      window.padding = {
        x = 5;
        y = 0;
      };
      scrolling.history = 10000;

      font = {
        normal.family = font;
        bold.family = font;
        italic.family = font;
        size = 10;
        # draw_bold_text_with_bright_colors = false;
      };
      scrolling.multiplier = 5;
      selection.save_to_clipboard = true;

    };
  };
}
