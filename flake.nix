{
  description = "NixOS configuration of Dennis Wenger";

  # ...

  inputs = {
    # ...
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    nixpkgs-stable.url = "github:nixos/nixpkgs/nixos-23.11";
    home-manager.url = "github:nix-community/home-manager";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";
    hyprland.url = "github:hyprwm/Hyprland";

    # Helix editor, using version 23.05
    # helix.url = "github:helix-editor/helix/23.05";
  };

  outputs = inputs@{ 
    self, 
    nixpkgs,
    nixpkgs-stable, 
    home-manager, 
    hyprland,
    ... 
  }: {
    nixosConfigurations = {

      nixos = nixpkgs.lib.nixosSystem rec {
        system = "x86_64-linux";

#        specialArgs = inputs;

        specialArgs = {
          self = self;
          home-manager = home-manager;
          pkgs-stable = import nixpkgs-stable {
            system = system;
            config.allowUnfree = true;
          };
          pkgs = import nixpkgs {
            system = system;
            config.allowUnfree = true;
          };
          hyprland = hyprland;
        };

        modules = [
          ./configuration.nix
          home-manager.nixosModules.home-manager
          {
            home-manager.useGlobalPkgs = true;
            home-manager.useUserPackages = true;

            home-manager.users.denniswenger = import ./home/default.nix;
          }
        ];
      };
    };
  };
}
